/*
Nama : Roy Bakti Surya Medal
Kelas: IF - 1B
NIM  : 301230061
*/

#include <iostream>
using namespace std;

int main (){

    // Input nilai
    cout << "Input Absen: "; cin >> absen;
    cout << "Input Tugas: "; cin >> tugas;
    cout << "Input Quiz: "; cin >> quiz;
    cout << "Input UTS: "; cin >> uts;
    cout << "Input UAS: "; cin >> uas;

    // Input Bobot Nilai
    double absenWeight, tugasWeight, quizWeight, utsWeight, uasWeight;
    cout << "Input Absen Weight: "; cin >> absenWeight;
    cout << "Input Tugas Weight: "; cin >> tugasWeight;
    cout << "Input Quiz Weight: "; cin >> quizWeight;
    cout << "Input UTS Weight: "; cin >> utsWeight;
    cout << "Input UAS Weight: "; cin >> uasWeight;

    // Kalkulator nilai
    nilai = ((absenWeight * absen) + (tugasWeight * tugas) + (quizWeight * quiz) + (utsWeight * uts) + (uasWeight * uas)) / 2;

    // Menentukan Huruf_Mutu
    if (nilai > 85 && nilai <= 100)
        Huruf_Mutu = 'A';
    else if (nilai > 70 && nilai <= 85)
        Huruf_Mutu = 'B';
    else if (nilai > 55 && nilai <= 70)
        Huruf_Mutu = 'C';
    else if (nilai > 40 && nilai <= 55)
        Huruf_Mutu = 'D';
    else if (nilai >= 0 && nilai <= 40)
        Huruf_Mutu = 'E';

    cout << "Huruf Mutu : " << Huruf_Mutu << endl;

    return 0;

}
